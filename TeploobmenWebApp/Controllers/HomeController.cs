﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Diagnostics;
using TeploLibrary;
using TeploobmenWebApp.Data;
using TeploobmenWebApp.Models;

namespace TeploobmenWebApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        private readonly ApplicationContext _context;

        private int _userId;

        public HomeController(ILogger<HomeController> logger, ApplicationContext applicationContext)
        {
            _logger = logger;
            _context = applicationContext;
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            int.TryParse(User.FindFirst("Id")?.Value, out _userId);
        }

        [HttpPost]
        public IActionResult Result(TeploobmenInput input)
        {
            //сохранение введенных данных
            if (!string.IsNullOrEmpty(input.Name))
            {
                var existVariant = _context.Variants.FirstOrDefault(x => x.Name == input.Name);

                if (existVariant != null)
                {
                    // Обновление варианта
                    existVariant.H = input.H;
                    existVariant.NTM = input.NTM;
                    existVariant.NTG = input.NTG;
                    existVariant.VG = input.VG;
                    existVariant.TG = input.TG;
                    existVariant.RM = input.RM;
                    existVariant.TM = input.TM;
                    existVariant.OKT = input.OKT;
                    existVariant.D  = input.D;

                    _context.Variants.Update(existVariant);
                    _context.SaveChanges();
                }
                else
                {
                    var variant = new Variant
                    {
                        Name = input.Name,
                        H = input.H,
                        NTM = input.NTM,
                        NTG = input.NTG,
                        VG = input.VG,
                        TG = input.TG,
                        RM = input.RM,
                        TM = input.TM,
                        OKT = input.OKT,
                        D = input.D,
                        UserId = _userId,
                        CreatedAt = DateTime.Now
                    };

                    _context.Variants.Add(variant);
                    _context.SaveChanges();
                }
            }
            //выполнение расчета
            var lib = new TeploLib(input);
            var result = lib.Res();
           
            return View(result);
        }

        [HttpGet]
        public IActionResult Index(int? variantId)
        {
            var viewModel = new HomeIndexViewModel();

            if (variantId != null)
            {
                viewModel.Variant = _context.Variants
                    .Where(x => x.UserId == _userId || x.UserId == 0)
                    .FirstOrDefault(x => x.Id == variantId);
            }

            viewModel.Variants = _context.Variants
                .Where(x => x.UserId == _userId || x.UserId == 0)
                .ToList();

            return View(viewModel);
        }

        [HttpGet]
        [Authorize]
        public IActionResult Remove(int? variantId)
        {

            var variant = _context.Variants
                .Where(x => x.UserId == _userId || x.UserId == 0)
                .FirstOrDefault(x => x.Id == variantId);

            if (variant != null)
            {
                _context.Variants.Remove(variant);
                _context.SaveChanges();

                TempData["message"] = $"Вариант {variant.Name} удален";
            }
            else
            {
                TempData["message"] = $"Вариант не найден";
            }

            return RedirectToAction(nameof(Index));
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}