﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Diagnostics;
using System.Security.Claims;
using TeploLibrary;
using TeploobmenWebApp.Data;
using TeploobmenWebApp.Models;

namespace TeploobmenWebApp.Controllers
{
    public class LoginController : Controller
    {
        private readonly ILogger<LoginController> _logger;

        private readonly ApplicationContext _context;

        [HttpGet]
        public IActionResult CreateDefaultUser()
        {
            var user = new User {Login = "admin",Password = "1111" };
            _context.Users.Add(user);
            _context.SaveChanges();

            return Ok();
        }

        public LoginController(ILogger<LoginController> logger, ApplicationContext applicationContext)
        {
            _logger = logger;
            _context = applicationContext;
        }

        [HttpPost]
        public async Task<IActionResult> Index(string login, string password)
        {
            var user = _context.Users.FirstOrDefault(x => x.Login == login && x.Password == password);

            if (user != null)
            {
                var claims = new List<Claim> {
                    new Claim("Id", user.Id.ToString()),
                    new Claim(ClaimTypes.Name, user.Login),
                };

                ClaimsIdentity claimsIdentity = new ClaimsIdentity(claims, "Cookies");

                await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(claimsIdentity));
                
                return RedirectToAction("Index", "Home");
            }

            return View();
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync();

            return RedirectToAction("Index");
        }


    }
}
