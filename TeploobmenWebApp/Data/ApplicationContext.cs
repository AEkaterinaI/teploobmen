﻿using Microsoft.EntityFrameworkCore;

namespace TeploobmenWebApp.Data
{
    public class ApplicationContext: DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Variant> Variants { get; set; }
        public ApplicationContext(DbContextOptions<ApplicationContext> options): base(options)
        {

        }
    }
}
