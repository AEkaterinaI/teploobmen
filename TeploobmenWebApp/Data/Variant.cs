﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TeploobmenWebApp.Data
{
    public class Variant
    {
        [Key]
        public int Id { get; set; }
        public int? UserId { get; set; }
        public string Name { get; set; }
        public double H { get; set; }
        public double NTM { get; set; }
        public double NTG { get; set; }
        public double VG { get; set; }
        public double TG { get; set; }
        public double RM { get; set; }
        public double TM { get; set; }
        public double OKT { get; set; }
        public double D { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime CreatedAt { get; set; }
    }
}
