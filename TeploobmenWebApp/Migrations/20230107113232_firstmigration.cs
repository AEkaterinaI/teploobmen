﻿ using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TeploobmenWebApp.Migrations
{
    /// <inheritdoc />
    public partial class firstmigration : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Login = table.Column<string>(type: "TEXT", nullable: false),
                    Password = table.Column<string>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Variants",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    UserId = table.Column<int>(type: "INTEGER", nullable: true),
                    Name = table.Column<string>(type: "TEXT", nullable: false),
                    H = table.Column<double>(type: "REAL", nullable: false),
                    NTM = table.Column<double>(type: "REAL", nullable: false),
                    NTG = table.Column<double>(type: "REAL", nullable: false),
                    VG = table.Column<double>(type: "REAL", nullable: false),
                    TG = table.Column<double>(type: "REAL", nullable: false),
                    RM = table.Column<double>(type: "REAL", nullable: false),
                    TM = table.Column<double>(type: "REAL", nullable: false),
                    OKT = table.Column<double>(type: "REAL", nullable: false),
                    D = table.Column<double>(type: "REAL", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Variants", x => x.Id);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Variants");
        }
    }
}
